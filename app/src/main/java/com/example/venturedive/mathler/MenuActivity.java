package com.example.venturedive.mathler;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Spinner;

public class MenuActivity extends AppCompatActivity {

    ListView menuListview;
    Spinner spinner;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menu);

        String[] menuArray= {"Play","Help","Setting","About"};

        spinner = (Spinner) findViewById(R.id.Spinner);
        menuListview =  (ListView) findViewById(R.id.menulist);


        ArrayAdapter<String> adapter
         = new ArrayAdapter<String>
        (MenuActivity.this,android.R.layout.simple_list_item_1,menuArray);
        spinner.setAdapter(adapter);

        menuListview.setAdapter(adapter);

    }
}
